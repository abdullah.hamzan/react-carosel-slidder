import { makeStyles } from '@material-ui/core';
import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Camera from "./assets/img/camera.jpg"
import Sepatu from "./assets/img/sepatu.jpg";
import Headset from "./assets/img/headset.jpg";
import JamTangan from "./assets/img/jamtangan.jpg";
import Lipstik from "./assets/img/lipstik.jpg";
import Speda from "./assets/img/sepeda.jpg";


const dataImage=[
  {
    id:1,
    data:Camera,
    name:"camera"
    
  },
  {
    id:2,
    data:Sepatu,
    name:"sepatu"
    
  },{
    id:3,
    data:Speda,
    name:"speda"
  },{
    id:4,
    data:JamTangan,
    name:"jam tangan"
  },{
    id:5,
    data:Headset,
    name:"headset"
  },{
    id:6,
    data:Lipstik,
    name:"lipstik"
  }
]


const useStyles=makeStyles({
  page:{
    width: "90%",
    height: "500px",
    margin: "5%",
    "& .slick-slide": {
      height:500,
      width: 400,
      boxSizing: "border-box",
      border:"1px solid #eaeaea",
      padding:"5px",
      borderRadius: "5px",
      "& .img":{
        borderRadius:"5px",
        border:"1px solid #eaeaea",
        width:"100%",
        height:500,
        objectFit:"cover"
    
      },
      "& .img:hover":{
        width:"100%",
        height:500,
        cursor:"pointer"
      },
      "& .slick-active":{
        border:"1px solid #F87474",
        "& .slick-cloned":{
          border:"1px solid #F87474",

        }

      }
  },

  }

})

const App = () => {

  const classes=useStyles();

  const settings = {
    dots: true,
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 3,
    speed: 500,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  return (
    <div className={classes.page}>
      <Slider {...settings}>
        {
          dataImage?.map((item,index)=>(
            <div key={index}>
            <img src={item.data} alt={item.name} className="img" />
           </div>


          ))
        }
  
      </Slider>


    </div>
  )
}

export default App